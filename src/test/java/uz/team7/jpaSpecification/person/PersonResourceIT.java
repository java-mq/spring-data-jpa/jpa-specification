package uz.team7.jpaSpecification.person;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonResourceIT {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private PersonRepository personRepository;

    private static UUID ID;
    private static final String NAME = "Mohammed";
    private static final Integer AGE = 20;
    private static final Gender GENDER = Gender.MALE;
    private static UUID FATHER_ID;

    private static final UUID ID_SIMULATION = UUID.randomUUID();
    private static final String NAME_SIMULATION = "Abdullah";
    private static final Integer AGE_SIMULATION = 21;
    private static final Gender GENDER_SIMULATION = Gender.FEMALE;
    private static final UUID FATHER_ID_SIMULATION = UUID.randomUUID();

    private static Person person;

    @BeforeEach
    public void setUp() {
        person = createEntity();
    }

    @Test
    @Transactional
    void testFilterByNothing() throws Exception {
        prepareData();

        mvc.perform(get("/api/persons"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").value(hasSize(1)))
                .andExpect(jsonPath("$.[*].id").value(hasItem(ID.toString())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(NAME)))
                .andExpect(jsonPath("$.[*].age").value(hasItem(AGE)))
                .andExpect(jsonPath("$.[*].gender").value(hasItem(GENDER.name())))
        ;
    }

    @Test
    @Transactional
    void testFilterById() throws Exception {
        prepareData();

        shouldBeFound("id.equals=" + ID);
        shouldNotBeFound("id.equals=" + ID_SIMULATION);

        shouldBeFound("id.notEquals=" + ID_SIMULATION);
        shouldNotBeFound("id.notEquals=" + ID);

        shouldBeFound("id.specified=" + true);
        shouldNotBeFound("id.specified=" + false);

        shouldBeFound("id.in=" + ID + "," + ID_SIMULATION);
        shouldNotBeFound("id.in=" + ID_SIMULATION);

        shouldBeFound("id.notIn=" + ID_SIMULATION);
        shouldNotBeFound("id.notIn=" + ID + "," + ID_SIMULATION);
    }

    @Test
    @Transactional
    void testFilterByName() throws Exception {
        prepareData();

        shouldBeFound("name.equals=" + NAME);
        shouldNotBeFound("name.equals=" + NAME_SIMULATION);

        shouldBeFound("name.notEquals=" + NAME_SIMULATION);
        shouldNotBeFound("name.notEquals=" + NAME);

        shouldBeFound("name.specified=" + true);
        shouldNotBeFound("name.specified=" + false);

        shouldBeFound("name.in=" + NAME + "," + NAME_SIMULATION);
        shouldNotBeFound("name.in=" + NAME_SIMULATION);

        shouldBeFound("name.notIn=" + NAME_SIMULATION);
        shouldNotBeFound("name.notIn=" + NAME + "," + NAME_SIMULATION);

        shouldBeFound("name.contains=" + NAME);
        shouldNotBeFound("name.contains=" + NAME_SIMULATION);

        shouldBeFound("name.doesNotContains=" + NAME_SIMULATION);
        shouldNotBeFound("name.doesNotContain=" + NAME);
    }

    @Test
    @Transactional
    void testFilterByAge() throws Exception {
        prepareData();

        shouldBeFound("age.equals=" + AGE);
        shouldNotBeFound("age.equals=" + AGE_SIMULATION);

        shouldBeFound("age.notEquals=" + AGE_SIMULATION);
        shouldNotBeFound("age.notEquals=" + AGE);

        shouldBeFound("age.specified=" + true);
        shouldNotBeFound("age.specified=" + false);

        shouldBeFound("age.in=" + AGE + "," + AGE_SIMULATION);
        shouldNotBeFound("age.in=" + AGE_SIMULATION);

        shouldBeFound("age.notIn=" + AGE_SIMULATION);
        shouldNotBeFound("age.notIn=" + AGE + "," + AGE_SIMULATION);

        shouldBeFound("age.greaterThan=" + (AGE - 1));
        shouldNotBeFound("age.greaterThan=" + AGE);
        shouldNotBeFound("age.greaterThan=" + (AGE + 1));

        shouldBeFound("age.lessThan=" + (AGE + 1));
        shouldNotBeFound("age.lessThan=" + AGE);
        shouldNotBeFound("age.lessThan=" + (AGE - 1));

        shouldBeFound("age.greaterThanOrEqual=" + (AGE - 1));
        shouldBeFound("age.greaterThanOrEqual=" + AGE);
        shouldNotBeFound("age.greaterThanOrEqual=" + (AGE + 1));

        shouldBeFound("age.lessThanOrEqual=" + (AGE + 1));
        shouldBeFound("age.lessThanOrEqual=" + AGE);
        shouldNotBeFound("age.lessThanOrEqual=" + (AGE - 1));
    }

    @Test
    @Transactional
    void testFilterByGender() throws Exception {
        prepareData();

        shouldBeFound("gender.equals=" + GENDER);
        shouldNotBeFound("gender.equals=" + GENDER_SIMULATION);

        shouldBeFound("gender.notEquals=" + GENDER_SIMULATION);
        shouldNotBeFound("gender.notEquals=" + GENDER);

        shouldBeFound("gender.specified=" + true);
        shouldNotBeFound("gender.specified=" + false);

        shouldBeFound("gender.in=" + GENDER + "," + GENDER_SIMULATION);
        shouldNotBeFound("gender.in=" + GENDER_SIMULATION);

        shouldBeFound("gender.notIn=" + GENDER_SIMULATION);
        shouldNotBeFound("gender.notIn=" + GENDER + "," + GENDER_SIMULATION);
    }

    @Test
    @Transactional
    void testFilterByFatherId() throws Exception {
        person.setFather(createEntity());
        personRepository.save(person);
        ID = person.getId();
        FATHER_ID = person.getFather().getId();

        shouldBeFound("fatherId.equals=" + FATHER_ID);
        shouldNotBeFound("fatherId.equals=" + FATHER_ID_SIMULATION);

        shouldBeFound("fatherId.notEquals=" + FATHER_ID_SIMULATION);
        shouldNotBeFound("fatherId.notEquals=" + FATHER_ID);

        shouldBeFound("fatherId.specified=" + true);
        shouldNotBeFound("fatherId.specified=" + false, 1);

        shouldBeFound("fatherId.in=" + FATHER_ID + "," + FATHER_ID_SIMULATION);
        shouldNotBeFound("fatherId.in=" + FATHER_ID_SIMULATION);

        shouldBeFound("fatherId.notIn=" + FATHER_ID_SIMULATION);
        shouldNotBeFound("fatherId.notIn=" + FATHER_ID + "," + FATHER_ID_SIMULATION);
    }

    void shouldBeFound(String filter, Integer expectedSize) throws Exception {
        mvc.perform(get("/api/persons?" + filter))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").value(hasSize(expectedSize)))
                .andExpect(jsonPath("$.[*].id").value(hasItem(ID.toString())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(NAME)))
                .andExpect(jsonPath("$.[*].age").value(hasItem(AGE)))
                .andExpect(jsonPath("$.[*].gender").value(hasItem(GENDER.name())))
        ;
    }

    void shouldNotBeFound(String filter, Integer expectedSize) throws Exception {
        mvc.perform(get("/api/persons?" + filter))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").value(hasSize(expectedSize)))
        ;
    }

    void shouldBeFound(String filter) throws Exception {
        shouldBeFound(filter, 1);
        ;
    }

    void shouldNotBeFound(String filter) throws Exception {
        shouldNotBeFound(filter, 0);
    }

    void prepareData() {
        personRepository.save(person);
        ID = person.getId();
    }

    Person createEntity() {
        return new Person()
                .setName(NAME)
                .setAge(AGE)
                .setGender(GENDER);
    }
}
