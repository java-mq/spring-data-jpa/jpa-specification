package uz.team7.jpaSpecification.person;

import jakarta.persistence.criteria.JoinType;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.team7.jpaSpecification.specification.AbsQueryService;

import java.util.List;

@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class PersonQueryService extends AbsQueryService<Person> {
    private final PersonRepository personRepository;

    @Transactional(readOnly = true)
    public List<Person> findByCriteria(PersonCriteria criteria) {
        return personRepository.findAll(createSpecification(criteria));
    }

    protected Specification<Person> createSpecification(PersonCriteria criteria) {
        Specification<Person> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = merge(specification, buildSpecification(criteria.getId(), Person_.id), criteria);
            }
            if (criteria.getName() != null) {
                specification = merge(specification, buildStringSpecification(criteria.getName(), Person_.name), criteria);
            }
            if (criteria.getAge() != null) {
                specification = merge(specification, buildRangeSpecification(criteria.getAge(), Person_.age), criteria);
            }
            if (criteria.getGender() != null) {
                specification = merge(specification, buildSpecification(criteria.getGender(), Person_.gender), criteria);
            }
            if (criteria.getFatherId() != null) {
                specification = merge(specification, buildSpecification(
                        criteria.getFatherId(), root -> root.join(Person_.father, JoinType.LEFT).get(Person_.id)), criteria
                );
            }
        }
        return specification;
    }
}
