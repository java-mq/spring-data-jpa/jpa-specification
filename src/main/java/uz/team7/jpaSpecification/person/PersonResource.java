package uz.team7.jpaSpecification.person;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class PersonResource {
    private final PersonQueryService personQueryService;

    @GetMapping("/persons")
    public List<Person> getPersons(PersonCriteria personCriteria) {
        return personQueryService.findByCriteria(personCriteria);
    }
}
