package uz.team7.jpaSpecification.person;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
@ToString
@EqualsAndHashCode
public class PersonDTO {
    private UUID id;

    @NotBlank
    private String name;

    @Positive
    private Integer age;

    @NotNull
    private Gender gender;

    private Person father;
}
