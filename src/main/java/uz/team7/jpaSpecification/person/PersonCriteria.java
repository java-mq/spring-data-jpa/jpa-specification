package uz.team7.jpaSpecification.person;

import lombok.Data;
import uz.team7.jpaSpecification.specification.CriteriaMarker;
import uz.team7.jpaSpecification.specification.filter.Filter;
import uz.team7.jpaSpecification.specification.filter.IntegerFilter;
import uz.team7.jpaSpecification.specification.filter.StringFilter;
import uz.team7.jpaSpecification.specification.filter.UUIDFilter;

@Data
public class PersonCriteria implements CriteriaMarker {
    private Boolean distinct;
    private Boolean search;

    private UUIDFilter id;
    private StringFilter name;
    private IntegerFilter age;
    private GenderFilter gender;
    private UUIDFilter fatherId;

    public static class GenderFilter extends Filter<Gender> {
    }
}
