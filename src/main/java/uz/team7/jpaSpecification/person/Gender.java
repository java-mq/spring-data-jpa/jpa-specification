package uz.team7.jpaSpecification.person;


/**
 * There are only two genders
 */
public enum Gender {
    MALE,
    FEMALE
}
