package uz.team7.jpaSpecification.specification;

public interface CriteriaMarker {
    Boolean getSearch();
}
