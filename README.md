Please, help me to improve my skills and share it with others.

Create a new branch, update/improve/refactor something and create MR (merge request). I will be very happy. 

[me in telegram](https://t.me/the_javer)

# What is here?

In this project, I used [spring jpa specification](https://docs.spring.io/spring-data/jpa/reference/jpa/specifications.html)
to filter data. Also, do not forget to check tests as well